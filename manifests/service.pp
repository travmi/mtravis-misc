# Class: misc::service
#
# This module manages misc services that need to be running or removed.
# These will be broken out into their own module at some point.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly. 
class misc::service {
  
  service { 'lvm2-monitor':
    ensure => 'stopped',
    enable => false,
  }
 
  service { 'rpcbind':
    ensure => 'running',
    enable => true
  }
  
  service { 'nfs':
    ensure => 'running',
    enable => true
  }
 
}