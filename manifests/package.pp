# == Class: misc::package
#
# This module manages misc packages, scripts, etc.
#
# === Parameters
#
# Parameters not used in this module.
#
class misc::package {
  
  package { 'wget':
    ensure => 'latest',
  }
  
  package { 'zip':
    ensure => 'latest',
  }
  
  package { 'unzip':
    ensure => 'latest',
  }
  
  package { 'openssl':
    ensure => 'latest',
  }
 
  package { 'curl':
    ensure => 'latest',
  }
  
  package { 'rsync':
    ensure => 'latest',
  }
  
  package { 'bind-utils':
    ensure => 'latest'
  }

  package { 'emacs':
    ensure => 'latest'
  }
  
  package { 'bash':
    ensure => 'latest'
  }
   
  package { 'iotop':
    ensure => 'latest'
  }
  
# The following perl packages need to be moved to their own module.
  package { 'perl':
    ensure => 'latest'
  }
 
  package { 'perl-libwww-perl':
    ensure => 'latest'
  }
  
  package { 'gcc':
    ensure => 'latest'
  }
 
  package { 'gcc-c++':
    ensure => 'latest'
  }

  package { 'make':
    ensure => 'latest'
  }
  
  package { 'mlocate':
    ensure => 'latest'
  }

  package { 'libssh2-devel':
    ensure => 'latest'
  } 

  package { 'libssh2':
    ensure => 'latest'
  }
  
  package { 'nfs-utils':
    ensure => 'latest'
  }
 
  package { 'rpcbind':
    ensure => 'latest'
  } 
  
  package { 'screen':
    ensure => 'latest'
  }
  
  package { 'lua':
    ensure => 'latest'
  }
  
  package { 'lua-devel':
    ensure => 'latest'
  }
  
  package { 'telnet':
    ensure => 'latest'
  }
  
}